package com.example.imagesearchapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.imagesearchapp.R;

public class SearchViewActivity extends AppCompatActivity {

	private EditText mEtKeyword;
	private Button mBtnSearch;
	public static final String KEY_WORD = "keyword";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_view);

		initView();
	}

	private void initView() {
		mEtKeyword = findViewById(R.id.et_keyword);
		mBtnSearch = findViewById(R.id.btn_search);
		mBtnSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				checkValid();
			}
		});
	}

	private void checkValid() {
		if(mEtKeyword.getText().length() <= 0 || mEtKeyword.getText() == null) {
			// TODO : alert
			return;
		}

		// go next view
		Intent intent = new Intent(SearchViewActivity.this, ResultViewActivity.class);
		intent.putExtra(KEY_WORD, mEtKeyword.getText().toString());
		startActivity(intent);
	}
}
