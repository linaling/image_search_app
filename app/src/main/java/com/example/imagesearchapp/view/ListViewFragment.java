package com.example.imagesearchapp.view;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.imagesearchapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListViewFragment extends Fragment {


	// Deatil 클릭시 : replace() 후 addToBackStack()
	public ListViewFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_list_view, container, false);
	}

}
