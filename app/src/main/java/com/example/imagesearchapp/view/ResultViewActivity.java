package com.example.imagesearchapp.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.imagesearchapp.R;
import com.example.imagesearchapp.vo.ImageVO;

import java.util.ArrayList;
import java.util.List;

public class ResultViewActivity extends AppCompatActivity {

	private Button mBtnGoBack;
	private TextView mTvKeyword;
	private RadioGroup mRgListStyle;
	private AppCompatRadioButton mRdoList;
	private AppCompatRadioButton mRdoTable;
	private RecyclerView mRvResultList;

	private ArrayList<ImageVO> mResultList = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result_view);

		initView();
		setContents();
	}

	private void initView() {
		mBtnGoBack = findViewById(R.id.btn_go_back);
		mBtnGoBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
		mTvKeyword = findViewById(R.id.tv_keyword);

		mRgListStyle = findViewById(R.id.rg_list_style);
		mRgListStyle.getChildCount();
		mRgListStyle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int i) {
				if(i == mRdoList.getId()) {
					// List
					mRvResultList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
				} else if(i == mRdoTable.getId()) {
					// Grid
					mRvResultList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
				}
			}
		});

		mRdoList = findViewById(R.id.rdo_list);
		mRdoTable = findViewById(R.id.rdo_table);
		mRvResultList = findViewById(R.id.rv_result_list);
		mRvResultList.setLayoutManager(new LinearLayoutManager(this));
	}

	private void setContents() {
		String keyword = getIntent().getStringExtra(SearchViewActivity.KEY_WORD);
		mTvKeyword.setText(keyword);

		mRdoList.setChecked(true);

		// set List
		setResultList();
		mRvResultList.setAdapter(new ResultAdapter(mResultList));
	}

	private class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ItemHolder> {

		private ArrayList<ImageVO> imageList = new ArrayList<>();

		class ItemHolder extends RecyclerView.ViewHolder {

			private TextView textView1;
			private TextView textView2;

			public ItemHolder(@NonNull View itemView) {
				super(itemView);

				textView1 = itemView.findViewById(R.id.tv_first);
				textView2 = itemView.findViewById(R.id.tv_second);
			}
		}

		public ResultAdapter(ArrayList<ImageVO> imageList) {
			this.imageList = imageList;
		}


		@NonNull
		@Override
		public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			Context context = parent.getContext();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.layout_image_item, parent, false);
			ItemHolder itemHolder = new ItemHolder(view);
			return itemHolder;
		}

		@Override
		public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
			ImageVO image = imageList.get(position);
			holder.textView1.setText(image.getImageName());
			holder.textView1.setText(image.getImageInfo());

			holder.itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					/*FragmentManager fm = getSupportFragmentManager();
					FragmentTransaction transaction = fm.beginTransaction();

					DetailViewFragment fragment = new DetailViewFragment();
					Bundle bundle = new Bundle();
					bundle.putString(SearchViewActivity.KEY_WORD, mTvKeyword.getText().toString());
					fragment.setArguments(bundle);

					transaction.add(R.id.ll_contents_view, fragment);
					transaction.commit();*/
				}
			});
		}

		@Override
		public int getItemCount() {
			return imageList.size();
		}
	}

	private void setResultList() {
		mResultList = new ArrayList<>();

		for(int i = 0; i < 20; i++) {

			ImageVO image = new ImageVO();
			image.setImageName("name : " + i);
			image.setImageInfo("info : " + i);
			mResultList.add(image);
		}
	}

}
