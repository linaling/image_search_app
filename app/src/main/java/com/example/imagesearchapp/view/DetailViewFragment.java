package com.example.imagesearchapp.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.imagesearchapp.R;

public class DetailViewFragment extends Fragment {

	String param = "";
	public DetailViewFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			param = getArguments().getString(SearchViewActivity.KEY_WORD);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_detail_view, container, false);
		TextView tvDetail = view.findViewById(R.id.tv_detail);
		tvDetail.setText(getArguments().getString(SearchViewActivity.KEY_WORD));
		return view;

	}
}
